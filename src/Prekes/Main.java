package Prekes;

import java.io.*;
import java.util.*;

public class Main {
    public static void main(String[] args) {
        String prekiuDuomenys = new File("").getAbsolutePath() +
                "/src/Prekes/Prekes.txt";
        String filtruotosPrekes = new File("").getAbsolutePath() +
                "/src/Prekes/FiltruotosPrekes.txt";
        List<Preke> prekes = skaitymas(prekiuDuomenys);
        Map<String, List<String>> mapas = prekesPagalTipa(prekes);
        meniu(scanneris(), prekiuDuomenys, prekes, mapas, filtruotosPrekes);
    }

    public static List<Preke> skaitymas(String file) {
        List<Preke> listas = new ArrayList<>();
        try (BufferedReader skaitytuvas = new BufferedReader(new FileReader(file))) {
            String eilute = skaitytuvas.readLine();
            while (eilute != null) {
                String[] duomenys = eilute.split(" ");
                Preke obj = null;
                if (duomenys[1].equals("televizoriai")) {
                    obj = new Televizorius(Integer.parseInt(duomenys[0]), duomenys[1], duomenys[2], Integer.parseInt(duomenys[3]),
                            Double.parseDouble(duomenys[4]), duomenys[5], duomenys[6]);
                } else if (duomenys[1].equals("nesiojamas-kompiuteris")) {
                    obj = new NesiojamasisKompiuteris(Integer.parseInt(duomenys[0]), duomenys[1], duomenys[2], Integer.parseInt(duomenys[3]),
                            Double.parseDouble(duomenys[4]), duomenys[5], duomenys[6], duomenys[7]);
                } else if (duomenys[1].equals("skalbykle") || duomenys[1].equals("dziovykle")) {
                    obj = new SkalbykleDziovykle(Integer.parseInt(duomenys[0]), duomenys[1], duomenys[2], Integer.parseInt(duomenys[3]),
                            Double.parseDouble(duomenys[4]), duomenys[5]);
                }
                listas.add(obj);
                eilute = skaitytuvas.readLine();
            }
        } catch (FileNotFoundException e) {
            System.out.println("Nerado failo");
            ;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return listas;
    }

    public static void meniu(Integer pasirinkimas, String file, List<Preke> prekes, Map<String, List<String>> mapas, String rezultatai) {
        Scanner sc = new Scanner(System.in);
        Integer prekesId = 0;
        switch (pasirinkimas) {
            case 1:
                System.out.println(prekes);
                break;
            case 2:
                System.out.println("Maziausiai vienetu: " + maziausiaiVienetu(prekes));
                break;
            case 3:
                System.out.println("Daugiausiai vienetu: " + daugiausiaiVienetu(prekes));
                break;
            case 4:
                rasymas(rezultatai, mapas);
                break;
            case 5:
                System.out.println("Pasirinkite kokia preke norit prideti:\n 1-Televizoriu, 2-Nesiojama kompiuteri, 3-Skalbykle 4 - dziovykle");
                Integer veiksmas = sc.nextInt();
                rasymasIPrekes(file, pridetiPrieListo(prekes, veiksmas));
                break;
            case 6:
                System.out.println("Iveskite prekes ID kuria norite istrinti:");
                prekesId = sc.nextInt();
                rasymasIPrekes(file, istrintIsListo(prekes, prekesId));
                break;
            case 7:
                System.out.println("Iveskite prekes ID kuria norite redaguoti:");
                prekesId = sc.nextInt();
                rasymasIPrekes(file, redaguotiPreke(prekes, prekesId));
                break;
            case 8:
                System.exit(0);
                break;
            default:
                System.out.println("Neteisingas pasirinkimas");
        }
        meniu(scanneris(), file, prekes, mapas, rezultatai);
    }

    public static Integer scanneris() {
        System.out.println("Pasirinkite veiksma:\n1 - Atspausdinti prekes i konsole.\n2 - Atspausdinti preke kuri turi maziausiai vienetu i konsole.\n" +
                "3 - Atspausdinti preke kuri turi daugiausiai vienetu i konsole.\n4 - Atspausdinti prekes pagal tipa i  txt faila.\n5 - Prideti preke.\n" +
                "6 - Istrinti preke.\n7 - Redaguoti preke.\n8 - Iseiti.");
        Scanner sc = new Scanner(System.in);
        Integer veiksmoSk = sc.nextInt();
        return veiksmoSk;
    }

    public static Preke maziausiaiVienetu(List<Preke> listas) {
        Preke minVnt = null;
        Integer min = Integer.MAX_VALUE;
        for (Preke obj : listas) {
            if (obj.getKiekis() < min) {
                min = obj.getKiekis();
                minVnt = obj;
            }
        }
        return minVnt;
    }

    public static Preke daugiausiaiVienetu(List<Preke> listas) {
        Preke maxVnt = null;
        Integer max = 0;
        for (Preke obj : listas) {
            if (obj.getKiekis() > max) {
                max = obj.getKiekis();
                maxVnt = obj;
            }
        }
        return maxVnt;
    }

    public static Map<String, List<String>> prekesPagalTipa(List<Preke> listas) {
        Map<String, List<String>> mapas = new HashMap<>();
        for (Preke obj : listas) {
            if(obj.getTipas().equals("televizoriai")) {
                Televizorius tv = (Televizorius)obj;
                if (mapas.containsKey(obj.getTipas())) {
                    mapas.get(obj.getTipas()).add(tv.getPavadinimas() + " " + tv.getKiekis() + " " + tv.getKaina() + " " + tv.getTechnologija() + " " + tv.getRaiska() + "\n");
                } else {
                    List<String> temp = new ArrayList<>();
                    temp.add(tv.getPavadinimas() + " " + tv.getKiekis() + " " + tv.getKaina() + " " + tv.getTechnologija() + " " + tv.getRaiska()  + "\n");
                    mapas.put(obj.getTipas(), temp);
                }
            } else if(obj.getTipas().equals("nesiojamas-kompiuteris")) {
                NesiojamasisKompiuteris nk = (NesiojamasisKompiuteris)obj;
                if (mapas.containsKey(obj.getTipas())) {
                    mapas.get(obj.getTipas()).add(nk.getPavadinimas() + " " + nk.getKiekis() + " " + nk.getKaina() + " " + nk.getProcesorius() + " " + nk.getRam() + " " + nk.getDiskoTalpa() + "\n");
                } else {
                    List<String> temp = new ArrayList<>();
                    temp.add(nk.getPavadinimas() + " " + nk.getKiekis() + " " + nk.getKaina() + " " + nk.getProcesorius() + " " + nk.getRam() + " " + nk.getDiskoTalpa() + "\n");
                    mapas.put(obj.getTipas(), temp);
                }
            }   else if(obj.getTipas().equals("dziovykle") || obj.getTipas().equals("skalbykle")) {
                SkalbykleDziovykle sd = (SkalbykleDziovykle)obj;
                if (mapas.containsKey(obj.getTipas())) {
                    mapas.get(obj.getTipas()).add(sd.getPavadinimas() + " " + sd.getKiekis() + " " + sd.getKaina() + " " + sd.getTalpa() + "\n");
                } else {
                    List<String> temp = new ArrayList<>();
                    temp.add(sd.getPavadinimas() + " " + sd.getKiekis() + " " + sd.getKaina() + " " + sd.getTalpa() + "\n");
                    mapas.put(obj.getTipas(), temp);
                }
            }
        }

        return mapas;
    }

    public static void rasymas(String rezultatai, Map<String, List<String>> mapas) {
        try (BufferedWriter rasytojas = new BufferedWriter(new FileWriter(rezultatai))) {
            rasytojas.write("Televizoriai:\n" + mapas.get("televizoriai"));
            rasytojas.write("\nNesiojami kompiuteriai:\n" + mapas.get("nesiojamas-kompiuteris"));
            rasytojas.write("\nSkalbimo masinos:\n" + mapas.get("skalbykle"));
            rasytojas.write("\nSkalbiniu dziovinimo masinos:\n" + mapas.get("dziovykle"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void rasymasIPrekes(String prekiuDuomenys, List<Preke> listas) {
        try (BufferedWriter rasytojas = new BufferedWriter(new FileWriter(prekiuDuomenys))) {
            Collections.sort(listas, (o1, o2) -> o1.getId().compareTo(o2.getId()));
            for(Preke obj : listas) {
                rasytojas.write(obj + "\n");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static List<Preke> pridetiPrieListo (List<Preke> listas, Integer veiksmas) {
        Integer id = listas.size() + 1;
        Preke obj = pasirinkimai(veiksmas, id);
        listas.add(obj);
        return listas;
    }

    public static Preke pasirinkimai(Integer pasirinkimas, Integer id){
        Scanner scanner = new Scanner(System.in);
        String pavadinimas, technologija, raiska, ram, kietasisDiskas, talpa, procesorius;
        Integer kiekis;
        Double kaina;
        System.out.println("Iveskite pavadinima:");
        pavadinimas = scanner.next();
        System.out.println("Iveskite kieki:");
        kiekis = scanner.nextInt();
        System.out.println("Iveskite kaina:");
        kaina = scanner.nextDouble();
        Preke obj;
        switch (pasirinkimas) {
            case 1:
                System.out.println("Iveskite technologija:");
                technologija = scanner.next();
                System.out.println("Iveskite raiska:");
                raiska = scanner.next();
                obj = new Televizorius(id, "televizoriai", pavadinimas, kiekis, kaina, technologija, raiska);
                return obj;
            case 2:
                System.out.println("Iveskite procesoriu:");
                procesorius = scanner.next();
                System.out.println("Iveskite ram:");
                ram = scanner.next();
                System.out.println("Iveskite kietaji diska:");
                kietasisDiskas = scanner.next();
                obj = new NesiojamasisKompiuteris(id, "nesiojamas-kompiuteris", pavadinimas, kiekis, kaina, procesorius, ram, kietasisDiskas);
                return obj;
            case 3:
                System.out.println("Iveskite talpa:");
                talpa = scanner.next();
                obj = new SkalbykleDziovykle(id, "skalbykle",  pavadinimas, kiekis, kaina, talpa);
                return obj;
            case 4:
                System.out.println("Iveskite talpa:");
                talpa = scanner.next();
                obj = new SkalbykleDziovykle(id, "dziovykle",  pavadinimas, kiekis, kaina, talpa);
                return obj;
            default:
                System.out.println("Neteisingas pasirinkimas");
        }
        return null;
    }

    public static List<Preke> istrintIsListo (List<Preke> listas, Integer id) {
        ListIterator<Preke> iterator = listas.listIterator();
        while(iterator.hasNext()) {
            if(iterator.next().getId() == id) {
                iterator.remove();
                listas = idKeitimas(listas, id-1);
                break;
            }
        }
        return listas;
    }

    public static List<Preke> redaguotiPreke (List<Preke> listas, Integer id) {
        for(int i = 0; i < listas.size(); i++) {
            if(id == listas.get(i).getId()) {
                if(listas.get(i) instanceof NesiojamasisKompiuteris){
                    System.out.println(listas.get(i));
                    Integer idRedagObj = listas.get(i).getId();
                    Preke redagObj = pasirinkimai( 2, idRedagObj);
                    listas.remove(listas.get(i));
                    listas.add(redagObj);
                }
                if(listas.get(i) instanceof Televizorius){
                    System.out.println(listas.get(i));
                    Integer idRedagObj = listas.get(i).getId();
                    Preke redagObj = pasirinkimai(1, idRedagObj);
                    listas.remove(listas.get(i));
                    listas.add(redagObj);
                }
                if(listas.get(i) instanceof SkalbykleDziovykle){
                    System.out.println(listas.get(i));
                    Integer idRedagObj = listas.get(i).getId();
                    Preke redagObj;
                    if(listas.get(i).getTipas().equals("skalbykle")) {
                        redagObj = pasirinkimai(3, idRedagObj);
                    } else {
                        redagObj = pasirinkimai(4, idRedagObj);
                    }
                    listas.remove(listas.get(i));
                    listas.add(redagObj);
                }
            }
        }
        return listas;
    }

    public static List<Preke> idKeitimas(List<Preke> list, Integer i) {
        while(i < list.size()) {

            list.get(i).setId(i+1);
            i++;
        }
        return list;
    }
}
