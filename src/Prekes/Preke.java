package Prekes;

public abstract class Preke {
    private Integer id;
    private String tipas;
    private String pavadinimas;
    private Integer kiekis;
    private Double kaina;

    @Override
    public String toString() {
        return "Preke{" +
                "id=" + id +
                ", pavadinimas='" + pavadinimas + '\'' +
                ", tipas='" + tipas + '\'' +
                ", kiekis=" + kiekis +
                ", kaina=" + kaina +
                '}';
    }

    public Integer getKiekis() {
        return kiekis;
    }

    public void setKiekis(Integer kiekis) {
        this.kiekis = kiekis;
    }

    public Double getKaina() {
        return kaina;
    }

    public void setKaina(Double kaina) {
        this.kaina = kaina;
    }

    public Preke(Integer id, String tipas, String pavadinimas, Integer kiekis, Double kaina) {
        this.id = id;
        this.pavadinimas = pavadinimas;
        this.tipas = tipas;
        this.kiekis = kiekis;
        this.kaina = kaina;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPavadinimas() {
        return pavadinimas;
    }

    public void setPavadinimas(String pavadinimas) {
        this.pavadinimas = pavadinimas;
    }

    public String getTipas() {
        return tipas;
    }

    public void setTipas(String tipas) {
        this.tipas = tipas;
    }
}
