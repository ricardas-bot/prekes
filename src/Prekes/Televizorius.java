package Prekes;

public class Televizorius extends Preke{
    private String technologija;
    private String raiska;

    public Televizorius(Integer id, String pavadinimas, String tipas, Integer kiekis, Double kaina, String technologija, String raiska) {
        super(id, pavadinimas, tipas, kiekis, kaina);
        this.technologija = technologija;
        this.raiska = raiska;
    }

    @Override
    public String toString() {
        return getId()+ " " + getTipas()+ " " + getPavadinimas() + " " + getKiekis() + " " + getKaina() + " " + technologija + " " + raiska;
    }

    public String getTechnologija() {
        return technologija;
    }

    public String getRaiska() {
        return raiska;
    }

    public void setTechnologija(String technologija) {
        this.technologija = technologija;
    }

    public void setRaiska(String raiska) {
        this.raiska = raiska;
    }
}
