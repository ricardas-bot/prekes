package Prekes;

public class SkalbykleDziovykle extends Preke{
    private String talpa;

    public SkalbykleDziovykle(Integer id, String pavadinimas, String tipas, Integer kiekis, Double kaina, String talpa) {
        super(id, pavadinimas, tipas, kiekis, kaina);
        this.talpa = talpa;
    }

    @Override
    public String toString() {
        return getId()+ " " + getTipas()+ " " + getPavadinimas() + " " + getKiekis() + " " + getKaina() + " " + talpa;
    }

    public String getTalpa() {
        return talpa;
    }

    public void setTalpa(String talpa) {
        this.talpa = talpa;
    }
}
