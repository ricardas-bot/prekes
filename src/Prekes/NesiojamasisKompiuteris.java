package Prekes;

public class NesiojamasisKompiuteris extends Preke{
    private String procesorius;
    private String ram;
    private String diskoTalpa;

    public NesiojamasisKompiuteris(Integer id, String pavadinimas, String tipas, Integer kiekis, Double kaina, String procesorius, String ram, String diskoTalpa) {
        super(id, pavadinimas, tipas, kiekis, kaina);
        this.procesorius = procesorius;
        this.ram = ram;
        this.diskoTalpa = diskoTalpa;
    }

    @Override
    public String toString() {
        return getId()+ " " + getTipas()+ " " + getPavadinimas() + " " + getKiekis() + " " + getKaina() + " " + procesorius + " " + ram + " " + diskoTalpa;
    }

    public String getProcesorius() {
        return procesorius;
    }

    public void setProcesorius(String procesorius) {
        this.procesorius = procesorius;
    }

    public String getRam() {
        return ram;
    }

    public void setRam(String ram) {
        this.ram = ram;
    }

    public String getDiskoTalpa() {
        return diskoTalpa;
    }

    public void setDiskoTalpa(String diskoTalpa) {
        this.diskoTalpa = diskoTalpa;
    }
}
